import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:personallawyer/PageView.dart';
import 'package:personallawyer/paybox/url_launcher.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'lang.dart';

class seccessfulPublicationScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return seccessfulPublicationScreenState();
  }
}

class seccessfulPublicationScreenState extends State<seccessfulPublicationScreen> {

  String url;
  var data;
  var status;
  String _api;
  var isLoading = true;
  String _lang;

  void Pay() async {

    final SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString('apikey');
    var id = sp.getString('id');

    print(id);

    url = "https://advokat.vipsite.kz/api/$_api/payment/api_paybox_payment/$id/questions/";

    print(url);

    final response = await http.get(url);

    data = jsonDecode(response.body);
    print(data);

    setState(() {
      data;

      Navigator.push(context, MaterialPageRoute(builder: (context) => LaunchUrl(url: data,)));

    });

  }

  void checkStatus() async {

    final SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString('apikey');
    var id = sp.getString('id');

    print(id);

    url = "https://advokat.vipsite.kz/api/$_api/payment/paybox_status_ok/$id/questions/";

    print(url);

    final response = await http.get(url);

    status = jsonDecode(response.body);
    print(status);

    setState(() {
      status;
      isLoading = false;
    });

  }

  void getLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _lang = prefs.getString("language");

    setState(() {
      _lang;
    });
  }

  void initState() {
    super.initState();
    checkStatus();
    getLang();
  }

  @override
  Widget build(BuildContext context) {
    getLang();
    return  isLoading
        ? Center(
      child: CircularProgressIndicator(),
    ) :
    status == 'partial' ?
    Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData (
            color: Colors.white,
          ),

          leading: Text(''),

          title: Text(trans('you_did_not_pay_the_bill', _lang), style: TextStyle(color: Colors.white)),
        ),

        body:
        SingleChildScrollView(
            child: Container (
              // padding: EdgeInsets.only(top: 10.0,),
              child: Column(
                children: <Widget>[

                  Container(
                    padding: EdgeInsets.only(top: 50, bottom: 45),
                    child: Column(
                      children: <Widget>[
                        Image.asset("assets/close.png", width: 170, height: 170,),
                      ],
                    ),
                  ),

                  Align(
                    child: Text(trans('you_did_not_pay_the_bill', _lang),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 17,
                        fontFamily: 'MyriadProRegular',
                        color: Colors.red,
                      ),) ,
                  ),

                  Container(
                    padding: EdgeInsets.only(top: 15),
                    child: Align(
                      child: Text(trans('info_text2', _lang),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 17,
                          fontFamily: 'MyriadProRegular',
                        ),) ,
                    ),
                  ),

                  InkWell(
                    child: Container(
                        margin: EdgeInsets.only(top: 25),
                        width: 220,
                        padding: EdgeInsets.all(12),
                        decoration: BoxDecoration(
                          color: Color(0xff63ad22),
                          borderRadius: BorderRadius.circular(50),
                        ),
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(trans('to_pay', _lang), style: TextStyle(color: Colors.white),),
                        )
                    ),
                    onTap: (){
                      Pay();
                    },
                  ),

                  InkWell(
                    child: Container(
                        margin: EdgeInsets.only(top: 15),
                        width: 220,
                        padding: EdgeInsets.all(12),
                        decoration: BoxDecoration(
                          color: Colors.red,
                          borderRadius: BorderRadius.circular(50),
                        ),
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(trans('cancel_the_application', _lang), style: TextStyle(color: Colors.white),),
                        )
                    ),
                    onTap: (){
                      Navigator.pushReplacement(context, MaterialPageRoute(
                          builder: (context) => PagerScreen()
                      ),);
                    },
                  ),

                ],
              ),
            )
        )
    )
        :
    Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData (
            color: Colors.white,
          ),

          leading: Text(''),

          title: Text(trans('successful_post', _lang), style: TextStyle(color: Colors.white)),
        ),

        body:  SingleChildScrollView(
            child: Container (
              // padding: EdgeInsets.only(top: 10.0,),
              child: Column(
                children: <Widget>[

                  Container(
                    padding: EdgeInsets.only(top: 50, bottom: 45),
                    child: Column(
                      children: <Widget>[
                        Image.asset("assets/q.png", width: 200, height: 200,),
                      ],
                    ),
                  ),

                  Align(
                    child: Text(trans('your_question_has_been_successfully_posted', _lang),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 17,
                          fontFamily: 'MyriadProRegular',
                          color: Color(0xff63ad22),
                      ),) ,
                  ),

                  Container(
                    padding: EdgeInsets.only(top: 15),
                    child: Align(
                      child: Text(trans('info_text3', _lang),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 17,
                          fontFamily: 'MyriadProRegular',
                        ),) ,
                    ),
                  ),

               /*   InkWell(
                    child: Container(
                        margin: EdgeInsets.only(top: 25),
                        width: 200,
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          color: Color(0xff63ad22),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Align(
                          alignment: Alignment.center,
                          child: Text('Оплатить', style: TextStyle(color: Colors.white),),
                        )
                    ),
                    onTap: (){
                      Pay();
                    },
                  ), */

                  InkWell(
                    child: Container(
                        margin: EdgeInsets.only(top: 25),
                        width: 220,
                        padding: EdgeInsets.all(12),
                        decoration: BoxDecoration(
                          color: Color(0xff63ad22),
                          borderRadius: BorderRadius.circular(50),
                        ),
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(trans('go_to_main_page', _lang), style: TextStyle(color: Colors.white),),
                        )
                    ),
                    onTap: (){
                      Navigator.pushReplacement(context, MaterialPageRoute(
                          builder: (context) => PagerScreen()
                      ),);
                    },
                  ),

                ],
              ),
            )
        )
    );
  }
}