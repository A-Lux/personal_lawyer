import 'package:onesignal_flutter/onesignal_flutter.dart';

class NotificationBloc {

  void initOneSignal() async{
    OneSignal.shared.setLogLevel(OSLogLevel.verbose, OSLogLevel.none);

    OneSignal.shared.init("70631e06-e94f-4e8b-a74f-7588690a6546");
    OneSignal. shared .setInFocusDisplayType (OSNotificationDisplayType.none);
    await OneSignal.shared.promptUserForPushNotificationPermission(fallbackToSettings: true);


  }
}