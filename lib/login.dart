import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginScreenState();
  }
}

class LoginScreenState extends State<LoginScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Вход', style: TextStyle(color: Colors.white, fontFamily: 'MyriadProRegular'), textAlign: TextAlign.center,),
        ),

        body:  SingleChildScrollView(
            child: Column(
              children: <Widget>[

                Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    padding: EdgeInsets.only(top: 20, right: 20, left: 20, bottom: 20),
                    child:  Text('Личный адвокат', style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold, fontFamily: 'MyriadProRegular')),
                  ),
                ),

               new SizedBox(
                 height: 10.0,
                 child: new Center(
                   child: new Container(
                     margin: new EdgeInsetsDirectional.only(start: 1.0, end: 1.0),
                     height: 1,
                     color: Colors.grey,
                   ),
                 ),
               ),

               Container(
                 padding: EdgeInsets.only(top: 5 ),
                 child:  Column(
                   children: <Widget>[

                  Container(
                    child: TextField(
                      style: TextStyle(
                        fontSize: 18.0,
                        color: Colors.grey,
                      ),
                      // controller: _phoneC,
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                          hintText: "E-mail",
                          hintStyle: TextStyle(
                            color: Colors.grey,
                            fontSize: 18.0,
                          ),
                          //                           border: InputBorder.none,
                          //labelText: _phone,
                          contentPadding: EdgeInsets.only(top: 5, right: 10, left: 15, bottom: 15),
                          labelStyle: TextStyle(
                            color: Colors.grey,
                            fontWeight: FontWeight.w100,
                            fontSize: 18.0,
                          )),
                    ),
                  ),

                  Container(
                    padding: EdgeInsets.only(top: 10),
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      style: TextStyle(
                        fontSize: 18.0,
                        color: Colors.grey,
                      ),
                      // controller: _smsC,
                      obscureText: true,
                      decoration: InputDecoration(
                          hintText: "Пароль",
                          hintStyle: TextStyle(
                            color: Colors.grey,
                            fontSize: 18.0,
                          ),
                          //                           border: InputBorder.none,
                          //labelText: _pass,
                          contentPadding: EdgeInsets.only(top: 5, right: 10, left: 15, bottom: 15),
                          labelStyle: TextStyle(
                            fontWeight: FontWeight.w100,
                            color: Colors.grey,
                            fontSize: 18.0,
                          )
                      ),
                    ),
                  )

                   ],
                 ),
               ),

            Container(
              padding: EdgeInsets.only(top: 30),
              child: ButtonTheme(
                minWidth: 120,
                height: 50,
                child:RaisedButton(
                  color: Colors.green,
                  child: Text('Войти', style: TextStyle(color: Colors.white, fontFamily: 'MyriadProRegular'),),
                ),
              )
            ),


              Container(
                padding: EdgeInsets.only(top: 30),
                child: Text('Войти с помощью соц. сетей', style: TextStyle(fontFamily: 'MyriadProRegular'),),
              ),

              ],
            ),

        ),

        bottomNavigationBar: Container(
        padding: EdgeInsets.all(30),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[

            InkWell(
              child: Text('Регистрация'),
              onTap: () {

              },
            ),

            InkWell(
              child: Text('Забыли пароль?', style: TextStyle(),),
              onTap: () {

              },
            ),

          ],

        )

        ),

    );
  }


}