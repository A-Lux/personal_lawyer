import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../PageView.dart';
import '../lang.dart';
import '../seccessfulPublication.dart';
class PayCard extends StatefulWidget {
  final htmlData;
  final isDocumentSale;

  const PayCard({Key key, this.htmlData, this.isDocumentSale}) : super(key: key);
  @override
  _PayCardState createState() => _PayCardState();
}

class _PayCardState extends State<PayCard> {
  final flutterWebViewPlugin = FlutterWebviewPlugin();
  bool isEnded;
  String _lang;
  bool fail;
  void getLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _lang = prefs.getString("language");

    setState(() {
      _lang;
    });
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getLang();
    isEnded = false;
    flutterWebViewPlugin.onUrlChanged.listen((String url) {
      print(url);
      if (url.contains('https://advokat.vipsite.kz/card/fail')) {
        print('wdwd');
        setState(() {
          isEnded = true;
          fail = true;

        });


      }
      if (url.contains('https://advokat.vipsite.kz/card/ready')) {
        setState(() {
          isEnded = true;
          fail = false;
        });


      }
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Оплата'),
        centerTitle: true,

      ),
      body: isEnded != false ?
      (fail == true ?
      SingleChildScrollView(
          child: Container (
            // padding: EdgeInsets.only(top: 10.0,),
            child: Column(
              children: <Widget>[

                Container(
                  padding: EdgeInsets.only(top: 50, bottom: 45),
                  child: Column(
                    children: <Widget>[
                      Image.asset("assets/close.png", width: 170, height: 170,),
                    ],
                  ),
                ),

                Align(
                  child: Text(trans('you_did_not_pay_the_bill', _lang),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 17,
                      fontFamily: 'MyriadProRegular',
                      color: Colors.red,
                    ),) ,
                ),

                Container(
                  padding: EdgeInsets.only(top: 15),
                  child: Align(
                    child: Text(trans('info_text2', _lang),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 17,
                        fontFamily: 'MyriadProRegular',
                      ),) ,
                  ),
                ),


                InkWell(
                  child: Container(
                      margin: EdgeInsets.only(top: 15),
                      width: 220,
                      padding: EdgeInsets.all(12),
                      decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(50),
                      ),
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(trans('cancel_the_application', _lang), style: TextStyle(color: Colors.white),),
                      )
                  ),
                  onTap: (){
                    Navigator.pushReplacement(context, MaterialPageRoute(
                        builder: (context) => PagerScreen()
                    ),);
                  },
                ),

              ],
            ),
          )
      )

          :
      SingleChildScrollView(
          child: Container (
            // padding: EdgeInsets.only(top: 10.0,),
            child: Column(
              children: <Widget>[

                Container(
                  padding: EdgeInsets.only(top: 50, bottom: 45),
                  child: Column(
                    children: <Widget>[
                      Image.asset("assets/q.png", width: 200, height: 200,),
                    ],
                  ),
                ),

                Align(
                  child: Text(this.widget.isDocumentSale ? trans('your_application_has_been_successfully_published', _lang) :trans('your_question_has_been_successfully_posted', _lang),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 17,
                      fontFamily: 'MyriadProRegular',
                      color: Color(0xff63ad22),
                    ),) ,
                ),

                Container(
                  padding: EdgeInsets.only(top: 15),
                  child: Align(
                    child: Text(this.widget.isDocumentSale ? trans('info_text3',_lang) : trans('info_text3', _lang),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 17,
                        fontFamily: 'MyriadProRegular',
                      ),) ,
                  ),
                ),

                /*   InkWell(
                    child: Container(
                        margin: EdgeInsets.only(top: 25),
                        width: 200,
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          color: Color(0xff63ad22),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Align(
                          alignment: Alignment.center,
                          child: Text('Оплатить', style: TextStyle(color: Colors.white),),
                        )
                    ),
                    onTap: (){
                      Pay();
                    },
                  ), */

                InkWell(
                  child: Container(
                      margin: EdgeInsets.only(top: 25),
                      width: 220,
                      padding: EdgeInsets.all(12),
                      decoration: BoxDecoration(
                        color: Color(0xff63ad22),
                        borderRadius: BorderRadius.circular(50),
                      ),
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(trans('go_to_main_page', _lang), style: TextStyle(color: Colors.white),),
                      )
                  ),
                  onTap: (){
                    Navigator.pushReplacement(context, MaterialPageRoute(
                        builder: (context) => PagerScreen()
                    ),);
                  },
                ),

              ],
            ),
          )
      ))
          :

      WebviewScaffold(
        url: new Uri.dataFromString(this.widget.htmlData, mimeType: 'text/html').toString(),

      ),

    );
  }
}
