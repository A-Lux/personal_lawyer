import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:personallawyer/AskQuestion_step3.dart';
import 'package:personallawyer/seccessfulPublication.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import '../lang.dart';

class LaunchUrl extends StatefulWidget{

  final String url;
  final bool isCardLink;

  LaunchUrl({Key key, @required this.url, this.isCardLink}): super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LaunchUrlState();
  }

}



class LaunchUrlState extends State<LaunchUrl> {

  final fwvp = new FlutterWebviewPlugin();
  bool done  = false;
  bool call = false;
  String _lang;
  String _num;
  String _call = "";
  final flutterWebViewPlugin = FlutterWebviewPlugin();

  void getLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _lang = prefs.getString("language");

    setState(() {
      _lang;
    });

  }
  bool isEnded = false;
  bool fail = false;

  @override
  void initState() {
    // TODO: implement initState
    startCounter();
    super.initState();
    getLang();

    flutterWebViewPlugin.onUrlChanged.listen((String url) {
      print(url);
      if (url.contains('https://advokat.vipsite.kz/card/fail')) {
        print('wdwd');



      }
      if (url.contains('https://advokat.vipsite.kz/login')) {
        Navigator.pop(context);

      }
    });
  }




  @override
  Widget build(BuildContext context) {
    getLang();
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData (
          color: Colors.white,
        ),
        title: Text(trans('payment', _lang), style: TextStyle(color: Colors.white)),

        leading: InkWell(
          child: Icon(Icons.arrow_back_ios),
          onTap: (){
            Navigator.pop(context);
          },
        ),

      ),

      body:
      Column(
        children: <Widget>[
          Container(
            child: done
                ? Container()
                : LinearProgressIndicator(),
          ),
          Container(
             width: MediaQuery.of(context).size.width,
             height: MediaQuery.of(context).size.height - 100,
              child: WebviewScaffold(
                url: widget.url,
                withZoom: true,
              )
          ),
        ],
      )
    );
  }

  void startCounter() {
    Future.delayed(const Duration(seconds: 4), () {
      setState((){
        done = true;
      });
    });
  }
}