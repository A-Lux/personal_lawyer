import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:personallawyer/paybox/url_launcher_document.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'lang.dart';

class DocumentShowPage extends StatefulWidget {
  final document;

  const DocumentShowPage({Key key, this.document}) : super(key: key);
  @override
  _DocumentShowPageState createState() => _DocumentShowPageState();
}

class _DocumentShowPageState extends State<DocumentShowPage> {
  String _lang;
  String _api;
  void initPage()async{
    SharedPreferences sp = await SharedPreferences.getInstance();
    _lang = sp.getString("language");
    _api = sp.getString("apikey");

  }

  void initPayment()async{
    var response = await http.get('https://advokat.vipsite.kz/api/document/get-payment-url/${this.widget.document['id']}/$_api');

    var body = jsonDecode(response.body);
    print(body);

    Navigator.push(context, MaterialPageRoute(builder: (context)=>LaunchUrlDocument(url: body['url'],)));
  }
  @override
  void initState() {
    initPage();

    // TODO: implement initState
    super.initState();

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Документ №${this.widget.document['id']}'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal:15.0,vertical: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: Text('${this.widget.document['doc_name']}',style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w500
              ),),

            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              child: RichText(
                text: TextSpan(
                  text: 'Тип документа:  ',
                  style: TextStyle(
                    color: Colors.grey
                  ),
                  children: [
                    TextSpan(
                      text: '${this.widget.document['doc_status']}',
                      style: TextStyle(
                        color: Colors.black
                      )
                    )
                  ]
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              child: RichText(
                text: TextSpan(
                    text: 'Цена:  ',
                    style: TextStyle(
                        color: Colors.grey
                    ),
                    children: [
                      TextSpan(
                          text: '${this.widget.document['doc_cost']} тенге',
                          style: TextStyle(
                              color: Colors.black
                          )
                      )
                    ]
                ),
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: Container(
        width: MediaQuery
            .of(context)
            .size
            .width,
        height: 60,
        decoration: BoxDecoration(
          color: Colors.green,
        ),
        child: FlatButton(
            onPressed: () {

              initPayment();
//              Navigator.push(
//                  context,
//                  MaterialPageRoute(
//                      builder: (context) => QuestionPay(price:_price)));
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.account_balance_wallet,
                    color: Colors.white
                ),
                Container(
                  padding: EdgeInsets.only(left: 15),
                  child: Text(
                      trans('to_pay', _lang),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 15
                      )
                  ),
                )
              ],
            )
        ),
      ),
    );
  }
}
