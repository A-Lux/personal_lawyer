import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:expandable/expandable.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'lang.dart';
import 'feedbackTopic.dart';
import 'goodRev.dart';

class AddClarificationScreen extends StatefulWidget {

  var answers_id;
  var user_question_id;

  AddClarificationScreen({Key key, @required this.answers_id, this.user_question_id}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return AddClarificationScreenState();
  }
}

class AddClarificationScreenState extends State<AddClarificationScreen> {

  TextEditingController textDesc = new TextEditingController();

  bool loading = false;
  void AddReviews() async {
    setState(() {
      loading = true;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var _api = prefs.getString("apikey");

    if(textDesc.text.isNotEmpty){
      var response = await http.post("https://advokat.vipsite.kz/api/question/add_clarification",
          body:{
            'api_token': _api,
            'user_question_id' : widget.user_question_id,
            'text_clarification': textDesc.text,
            'answers_id' : widget.answers_id,

          });

      print(widget.answers_id);


      print(response.statusCode);
      print(response.body);

      if(response.statusCode == 200){

        setState(() {
          loading = false;
        });
        var data = jsonDecode(response.body);

//        Fluttertoast.showToast(
//          msg: data,
//          toastLength: Toast.LENGTH_SHORT,
//          gravity: ToastGravity.BOTTOM,
//          timeInSecForIos: 5,
//          backgroundColor: Colors.green,
//          textColor: Colors.white,
//        );

        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => goodRevScreen()));

      }


    } else {
      Fluttertoast.showToast(
        msg: trans('fill_in_all_required_fields', _lang),
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
      );
    }

  }

  String _lang;

  void getLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _lang = prefs.getString("language");

    setState(() {
      _lang;
    });
  }

  void initState() {
    getLang();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    getLang();
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData (
            color: Colors.white,
          ),
          title: Text(trans('adding_clarification', _lang), style: TextStyle(color: Colors.white)),
        ),

        body:  SingleChildScrollView(
            child: Container (
              // padding: EdgeInsets.only(top: 10.0,),
              child: Column(
                children: <Widget>[

                  Container(
                      margin: EdgeInsets.all(20),
                      height: 200,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(width: 1, color: Color(0xff4267b2),),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      padding: EdgeInsets.all(10.0),
                      child: new ConstrainedBox(constraints: BoxConstraints(
                        maxHeight: 200.0,
                      ),
                        child: new Scrollbar(
                          child: new SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            //  reverse: true,
                            child: SizedBox(
                              height: 200.0,
                              child: new TextField(
                                textCapitalization: TextCapitalization.sentences,
                                textInputAction: TextInputAction.done,
                                controller: textDesc,
                                maxLines: 80,
                                decoration: new InputDecoration(
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                          ),
                        ),)

                  ),

                  InkWell(
                    child: Container(
                        width: 200,
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          color: Colors.green,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(trans('submit', _lang), style: TextStyle(color: Colors.white),),
                        )
                    ),
                    onTap: (){
                      if(loading == false){
                        AddReviews();
                      }
                    },
                  )

                ],
              ),
            )
        )
    );
  }
}