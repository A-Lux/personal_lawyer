import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:personallawyer/AskQuestion_step1.dart';
import 'package:personallawyer/AskQuestion_step2City.dart';
import 'package:personallawyer/finishedDocumentsPage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'AskQuestion_step3.dart';
import 'lang.dart';

class AskQuestion_Step2Screen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return AskQuestion_Step2ScreenState();
  }
}

class AskQuestion_Step2ScreenState extends State<AskQuestion_Step2Screen> {

  var data;
  String cityCity;
  String phoneUser;
  String nameTextInput;
  String emailTextInput;
  String user;
  String _lang;

  TextEditingController nameText = new TextEditingController();
  TextEditingController emailText = new TextEditingController();
  TextEditingController cityText = new TextEditingController();
  TextEditingController phoneText = new TextEditingController();

  void getCity() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    cityCity = sp.getString("cityText");
    phoneUser = sp.getString("phone_number_user");
    nameTextInput = sp.getString("nameText");
    emailTextInput = sp.getString("emailText");

    nameText.text = nameTextInput;
    emailText.text = emailTextInput;

    setState(() {
      cityCity;
      phoneUser;
      nameTextInput;
      emailTextInput;
      emailText;
      nameText;
    });

  }

  void setInfo() async {

    SharedPreferences sp = await SharedPreferences.getInstance();
    sp.setString('nameText', nameText.text);
    sp.setString('emailText', emailText.text);
   // sp.setString('cityText', cityCity);

  }



  void initState() {
    super.initState();
    getCity();
    getLang();
    print(cityCity);

  }



  void Add_Question_Step2 () async {

    if(nameText.text.isNotEmpty && emailText.text.isNotEmpty){

      SharedPreferences sp = await SharedPreferences.getInstance();
      sp.setString('cityText', cityCity);
      sp.setString('nameText', nameText.text);
      sp.setString('phoneText', phoneText.text);
      sp.setString('emailText', emailText.text);

    } else {
      Fluttertoast.showToast(
        msg: trans('please_fill_in_the_fields', _lang),
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
      );
    }

  }

  void getLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _lang = prefs.getString("language");

    setState(() {
      _lang;
    });
  }


  @override
  Widget build(BuildContext context) {
    getLang();
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData (
            color: Colors.white,
          ),
          title: Text(trans('about_you', _lang), style: TextStyle(color: Colors.white)),
          actions: <Widget>[
            FlatButton(
                child: Container(
                  child: Text(trans('step2of3', _lang), style: TextStyle(color: Colors.white),),
                )
            )
          ],
        ),

        body:  SingleChildScrollView(
            child: Container (
              padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 15),
              child: Column(
                children: <Widget>[

                  Container(
                      padding: EdgeInsets.only(top:5, bottom:10),
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        children: <Widget>[
                          Text(trans('full_name', _lang),
                            textAlign: TextAlign.start,
                            style: TextStyle(color: Colors.grey,
                                fontWeight: FontWeight.bold
                            ),
                          ),
                        ],
                      )
                  ),
                  Container(
                    child: TextField(
                      controller: nameText,
                      textCapitalization: TextCapitalization.sentences,
                      textInputAction: TextInputAction.done,
                      decoration: InputDecoration(
                        hintText: trans('field_to_fill', _lang),
                        filled: true,
                        fillColor: Colors.white,
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey[300]),
                        ),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xff4267b2), width: 0.75)
                        ),
                        //labelText: _title,
                        contentPadding: EdgeInsets.all(12.0),
                      ),
                      onChanged: (String text){

                      },
                    ),
                  ),

                  Container(
                      padding: EdgeInsets.only(top:15, bottom:10),
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        children: <Widget>[
                          Text("E-mail:",
                            textAlign: TextAlign.start,
                            style: TextStyle(color: Colors.grey,
                                fontWeight: FontWeight.bold
                            ),
                          ),
                        ],
                      )
                  ),
                  Container(
                    child: TextField(
                      controller: emailText,
                      textCapitalization: TextCapitalization.sentences,
                      textInputAction: TextInputAction.done,
                      decoration: InputDecoration(
                        hintText: trans('field_to_fill', _lang),
                        filled: true,
                        fillColor: Colors.white,
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey[300]),
                        ),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xff4267b2), width: 0.75)
                        ),
                        //labelText: _title,
                        contentPadding: EdgeInsets.all(12.0),
                      ),
                      onChanged: (String text){

                      },
                    ),
                  ),

                  Container(
            padding: EdgeInsets.only(top: 25, bottom: 10),
            child: InkWell(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  cityCity == null ? Text(trans('choose_a_city', _lang), style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),) : Container(child: Text(cityCity)),
                  Icon(Icons.keyboard_arrow_right, color: Colors.grey)
                ],
              ),
              onTap: (){
                setInfo();
                Navigator.pushReplacement(context,
                    MaterialPageRoute(
                        builder: (context) => AskQuestion_Step2CityScreen()));
              },
            )
          ),

                  Divider(height: 2,),

//                  Container(
//                      padding: EdgeInsets.only(top:15, bottom:10),
//                      width: MediaQuery.of(context).size.width,
//                      child: Row(
//                        children: <Widget>[
//                          Text("Город:",
//                            textAlign: TextAlign.start,
//                            style: TextStyle(color: Colors.grey,
//                                fontWeight: FontWeight.bold
//                            ),
//                          ),
//                        ],
//                      )
//                  ),
//                  Container(
//                    child: TextField(
//                      //  controller: _nameC,
//                      textCapitalization: TextCapitalization.words,
//                      textInputAction: TextInputAction.done,
//                      decoration: InputDecoration(
//                        hintText: '',
//                        hintStyle: TextStyle(color: Colors.grey[200]),
//                        filled: true,
//                        fillColor: Colors.white,
//                        enabledBorder: OutlineInputBorder(
//                          borderSide: BorderSide(color: Colors.grey[300]),
//                        ),
//                        focusedBorder: OutlineInputBorder(
//                            borderSide: BorderSide(color: Color(0xff4267b2), width: 0.75)
//                        ),
//                        //labelText: _title,
//                        contentPadding: EdgeInsets.all(12.0),
//                      ),
//                      onChanged: (String text){
//
//                      },
//                    ),
//                  ),

                  Container(
                      padding: EdgeInsets.only(top:15, bottom:10),
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        children: <Widget>[
                          Text(trans('phone', _lang),
                            textAlign: TextAlign.start,
                            style: TextStyle(color: Colors.grey,
                                fontWeight: FontWeight.bold
                            ),
                          ),
                        ],
                      )
                  ),
                  Container(
                    child: TextField(
                      controller: phoneText,
                      enabled: false,
                      textCapitalization: TextCapitalization.sentences,
                      textInputAction: TextInputAction.done,
                      decoration: InputDecoration(
                        hintText: phoneUser,
                        hintStyle: TextStyle(color: Colors.grey),
                        filled: true,
                        fillColor: Colors.white,
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey[300]),
                        ),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xff4267b2), width: 0.75)
                        ),
                        //labelText: _title,
                        contentPadding: EdgeInsets.all(12.0),
                      ),
                      onChanged: (String text){

                      },
                    ),
                  ),

                  Container(
                      padding: EdgeInsets.only(top:15, bottom:30),
                      child: Text(trans('info_text', _lang),
                        textAlign: TextAlign.start,
                        style: TextStyle(color: Colors.grey,
                            //fontWeight: FontWeight.bold
                        ),
                      ),
                  ),


                ],
              ),
            )
        ),

      bottomNavigationBar: Container(
        width: MediaQuery
            .of(context)
            .size
            .width,
        height: 60,
        decoration: BoxDecoration(
          color: Colors.green,
        ),
        child: FlatButton(
            onPressed: () {

              Add_Question_Step2();

              if(nameText.text.isNotEmpty && emailText.text.isNotEmpty){

                if(cityCity == null) {
                  Fluttertoast.showToast(
                    msg: trans('please_choose_your_city', _lang),
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.CENTER,
                    timeInSecForIos: 1,
                    backgroundColor: Colors.red,
                    textColor: Colors.white,
                  );
                } else {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AskQuestion_Step3Screen()));
                }

              }

            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[

                Container(
                  padding: EdgeInsets.only(right: 15),
                  child: Text(
                      trans('further', _lang),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 15
                      )
                  ),
                ),

                Icon(Icons.keyboard_arrow_right,
                    color: Colors.white
                ),
              ],
            )
        ),
      ),

    );
  }
}