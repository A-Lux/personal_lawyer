import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:personallawyer/DocumentShowPage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class DocumentsByCategory extends StatefulWidget {
  final category;

  const DocumentsByCategory({Key key, this.category}) : super(key: key);
  @override
  _DocumentsByCategoryState createState() => _DocumentsByCategoryState();
}

class _DocumentsByCategoryState extends State<DocumentsByCategory> {
  String _api;
  var url;
  var category;
  String _lang;
  var isLoading = true;
  List<Widget>myList = new List();
  ListView list;

  void getListCategory() async {

    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");
    _lang = sp.getString("language");
    String mainLang = '';
    if(_lang == 'rus'){
      mainLang = 'ru';
    }else{
      mainLang = 'kz';
    }
    url = "https://advokat.vipsite.kz/api/document/by-category/${this.widget.category['id']}/$mainLang/$_api";

    print(url);
    final response = await http.get(url);





    category = jsonDecode(response.body);

    category = category['data'];
    print(category);

    setState(() {
      category;
      isLoading = false;
    });

    for (int i = 0; i < category.length; i++) {
      myList.add(
          new Column(
            children: <Widget>[
              ListTile(
                title: Text(category[i]['doc_name']),
                trailing: Icon(Icons.keyboard_arrow_right, color: Colors.grey,),
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>DocumentShowPage(document: category[i],)));

                },
              ),
              Divider(height: 2,),
            ],
          )

      );
    }

  }


  void initState() {
    getListCategory();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('${this.widget.category['name']}'),
          centerTitle: true,
        ),
        body:  isLoading
            ? Center(
            child: CircularProgressIndicator())
            : SingleChildScrollView(
          child: Container(
            child: Column(
              children: myList,
            ),
          ),
        )

    );
  }
}
