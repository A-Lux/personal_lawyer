import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'lang.dart';

class RateTheApplicationScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return RateTheApplicationScreenState();
  }
}

class RateTheApplicationScreenState extends State<RateTheApplicationScreen>{

  String _lang;

  void getLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _lang = prefs.getString("language");

    setState(() {
      _lang;
    });
  }

  void initState() {
    getLang();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    getLang();
    return Scaffold(
      appBar: AppBar(
        title: Text(trans('rate_the application', _lang)),
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(bottom: 20.0),
                  child: Icon(Icons.mood_bad,
                    color: Colors.red,
                    size: 45.0,),
                ),
                Container(
                  child: RaisedButton(
                    color: Colors.white,
                    child: Text(trans('i_do_not_like', _lang),
                      style: TextStyle(
                          color: Colors.red
                      ),),
                    onPressed: (){

                    },
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 16.0, left: 45.0, right: 45.0),
                  child: Text(trans('info_text6', _lang),
                    textAlign: TextAlign.center,),
                ),
              ],
            ),
            Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(bottom: 20.0),
                  child: Icon(Icons.mood,
                    color: Colors.green,
                    size: 45.0,),
                ),
                Container(
                  child: RaisedButton(
                    color: Colors.white,
                    child: Text(trans('like', _lang),
                      style: TextStyle(
                          color: Colors.green
                      ),),
                    onPressed: (){

                    },
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 16.0, left: 45.0, right: 45.0),
                  child: Text(trans('info_text6', _lang),
                    textAlign: TextAlign.center,),
                ),
              ],
            ),

          ],
        ),
      ),
    );
  }
}