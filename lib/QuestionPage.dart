import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:expandable/expandable.dart';
import 'package:personallawyer/AddClarification.dart';
import 'package:personallawyer/lang.dart';
import 'package:personallawyer/userProfil.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'lang.dart';
import 'feedbackTopic.dart';

class QuestionPageScreen extends StatefulWidget {

  String id_question;

  QuestionPageScreen ({Key key, @required this.id_question}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return QuestionPageScreenState();
  }
}

class QuestionPageScreenState extends State<QuestionPageScreen> {

  String _api;
  var isLoading = true;
  var data;
  var url;
  var index;
  var clar;
  String _lang;
  var add_answers;
  var add_answers_data;
  var temp_two;

  int dataq;

  int dataqq = 351;


  bool _text = false;

  var loading = false;

  List<Widget>myList = new List();
  ListView list;

  var user;

  void get_clarification_block(var value) {
    print("value = "+value); // Return block of clarification
  }

  void getInfoQuestion() async {

    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");
    _lang = sp.getString("language");

    url = "https://advokat.vipsite.kz/api/$_api/question/question_answers/${widget.id_question}/$_lang/";

    final response = await http.get(url);
    print(response.statusCode);

    if (response.statusCode == 200) {

      data = jsonDecode(response.body);
    //  print(data);
      var i = new Map.from(data);

      dataq = data["text_question"].length;

     // add_answers = new List.from(data["add_answers"]);


//      add_answers = new List.from(data["add_answers"]);

      if (data["add_answers"] != null) {
        add_answers = new List.from(data["add_answers"]);
      }

      print(data["mobile_users_id"]);

      setState(() {
        data;

        get_lawyer();

        isLoading = false;


      });


    } else {
      throw Exception('Failed to load files');

    }

  }

  void checkUser() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    _api = sp.getString("apikey");

    url = "https://advokat.vipsite.kz/api/$_api/about_you/user/user_data";

    final response = await http.get(url);

    user = jsonDecode(response.body);

  }

  void initState() {
    getInfoQuestion();
    checkUser();
    getLang();
    super.initState();
  }

  void get_lawyer() {

    print(data["mobile_users_id"]);

    if (add_answers != null) {
      var temp = new List.from(add_answers);

      for (int i = 0; i < temp.length; i++) {
        myList.add(
            new Container(
              //  padding: EdgeInsets.only(left: 20, top:15.0),
                child: Column(
                  children: <Widget>[

                    Container(
                      padding: EdgeInsets.only(
                          left: 15, top: 15.0 ),
                      child: Row(
                        children: <Widget>[
                          CircularProfileAvatar(
                            'https://advokat.vipsite.kz/' +
                                temp[i]['image_link'],
                            radius: 35,
                            backgroundColor: Colors.transparent,
                            //borderWidth: 3,
                            //initialsText: Text("AD", style: TextStyle(fontSize: 40, color: Colors.white),),
                            borderColor: Colors.white,
                            elevation: 5.0,
                            //foregroundColor: Colors.white10.withOpacity(0.5),
                            cacheImage: true,
                            onTap: () {
                              // getImage(context);
                            },
                            showInitialTextAbovePicture: true,
                          ),
                          Container(
                            padding: EdgeInsets.only(
                                left: 15 ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                InkWell(
                                  child: Container(
                                    child: Text(
                                        temp[i]['name'] ),
                                  ),
                                  onTap: () {
                                    Navigator.pushReplacement(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                UserProfilScreen(
                                                    id: temp[i]['id'] ) ) );
                                  },
                                ),
                                Container(
                                  padding: EdgeInsets.only(
                                      top: 5 ),
                                  child: Text(
                                    temp[i]['city'], style: TextStyle(
                                      color: Colors.grey ), ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),


                    Container(
                      padding: EdgeInsets.only(
                          top: 15, left: 20, right: 20, bottom: 15 ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            child: Text(
                                temp[i]["answers"]["answers"] ),
                          ),
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(
                                      top: 10 ),
                                  child: Text(
                                    temp[i]["answers"]["created"],
                                    style: TextStyle(
                                        color: Colors.grey ), ),
                                ),

                                temp[i]["answers"]["clarification"] != null ?

                                Container() :

                                Column(
                                  children: <Widget>[

                                    InkWell(
                                      child: Container(
                                        padding: EdgeInsets.only(
                                            top: 10,
                                            bottom: 10,
                                            right: 20,
                                            left: 20 ),
                                        child: Row(
                                          children: <Widget>[
                                            Icon(
                                                Icons.receipt, color: Colors.blue ),
                                            Container(
                                              padding: EdgeInsets.only(
                                                  left: 5 ),
                                              child: Text(
                                                'Уточнить', style: TextStyle(
                                                  color: Colors.blue ), ),
                                            )
                                          ],
                                        ),
                                      ),
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    AddClarificationScreen(
                                                      answers_id: temp[i]["answers"]["id"]
                                                          .toString(
                                                      ),
                                                      user_question_id: data["id"]
                                                          .toString(
                                                      ), ) ) );
                                      },
                                    ),

                                  ],
                                )

                              ],
                            ),
                          )
                        ],
                      ),
                    ),

                    temp[i]["answers"]["clarification"] != null ?
                 Container(
                          padding: EdgeInsets.only(
                              top: 20, bottom: 15, left: 20, right: 20 ),
                          decoration: new BoxDecoration(
                            color: Colors.grey[200],
                          ),
                          width: MediaQuery
                              .of(
                              context )
                              .size
                              .width,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              Container(
                                child: Text(
                                  'Уточнение:', style: TextStyle(
                                    color: Colors.grey ), ),
                              ),
                              Container(
                                  padding: EdgeInsets.only(
                                      top: 5 ),
                                  child: Text(
                                      temp[i]["answers"]["clarification"]["text_clarification"] )
                              ),
                              Container(
                                padding: EdgeInsets.only(
                                    top: 5 ),
                                child: Text(
                                  temp[i]["answers"]["clarification"]["created"],
                                  style: TextStyle(
                                      color: Colors.grey ), ),
                              )
                            ],
                          ),
                        ) :
                    Container(
                      child: Text( '' ), ),
                    Divider(
                      height: 2.0, ),

                  ],
                )
            ) );
      }
    }

  }

  void getLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _lang = prefs.getString("language");

    setState(() {
      _lang;
    });
  }

  @override
  Widget build(BuildContext context) {
    getLang();
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData (
            color: Colors.white,
          ),
          title: Text(trans('question', _lang) + widget.id_question, style: TextStyle(color: Colors.white)),
        ),

        body: isLoading
            ? Center(
          child: CircularProgressIndicator(),
        )
            : SingleChildScrollView(
            child: Container (
              // padding: EdgeInsets.only(top: 10.0,),
             // height: MediaQuery.of(context).size.height,
              child: Column(
               // crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[

                        Container(
                          padding: EdgeInsets.only(top: 20, bottom: 10),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                child: Text(data["header_question"], style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),),
                              ),

                              Container(
                                padding: EdgeInsets.only(top: 10),
                                child: Text(data["city_question"], style: TextStyle(color: Colors.grey),),
                              ),

                              _text
                                  ? Container(
                                padding: EdgeInsets.only(top: 10),
                                child: Text(data["text_question"]),
                              )
                                  : Container(
                                padding: EdgeInsets.only(top: 10),
                                child: Text(data["text_question"],
                                maxLines: 7,
                                overflow: TextOverflow.ellipsis,),
                              ),


                              Container(
                                padding: EdgeInsets.only(top: 10),
                                child:  Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(data["created"], style: TextStyle(color: Colors.grey),),

                                    dataqq > dataq ?

                                    Container()

                                        :

                                    Container(
                                        child: FlatButton(
                                            onPressed: (){
                                              setState((){
                                                _text = !_text;
                                              });
                                            },
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: <Widget>[
                                                InkWell(
                                                  child: _text
                                                      ? Text(trans('collapse', _lang), style: TextStyle(color: Colors.blue),)
                                                      : Text(trans('expand', _lang), style: TextStyle(color: Colors.blue),),
                                                  onTap: (){
                                                    setState((){
                                                      _text = !_text;
                                                    });
                                                  },
                                                ),
                                              ],
                                            )
                                        )
                                    )

                                  ],
                                ),
                              ),

                            ],
                          ),
                        ),

                      ],
                    ),
                  ),


                  Divider(height: 2, color: Colors.grey,),

/*

                  add_answers == null ?

                  Container(
                    child: Column(
                      children: <Widget>[
                        Container(
                          color: Color(0xffe6e6e6),
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.only(top:15, bottom: 15),
                          child: Text('Ответы от юристов еще не поступили', textAlign: TextAlign.center,),
                        ),
                        Divider(height: 2, color: Colors.grey,),
                      ],
                    ),
                  )

                  :

                  Column(
                      children: myList,
                    ),

*/


                ],
              ),
            ),
        ),


    );

  }
}
